from QualiaPy.Module.LungSegmentation import *

__author__ = 'wchoi'

if __name__ == "__main__":
    lungSegmentation = LungSegmentation()
    lungSegmentation.set_input('test', 10)
    lungSegmentation.set_input('test1', 0)
    lungSegmentation.set_input('test2')
    lungSegmentation.set_input('test3', 1)
    lungSegmentation.start()
    # lungSegmentation.run()
    print(lungSegmentation.get_output(0))
    print(lungSegmentation.get_output(10))
    print(lungSegmentation.get_output())
    print(lungSegmentation.name)
