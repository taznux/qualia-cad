import SimpleITK as sitk

from .ModuleBase import ModuleBase

__author__ = 'wchoi'


class LungSegmentation(ModuleBase):
    # lung segmentation module class

    lung_image = sitk.Image()
    mask_image = sitk.Image()

    def __init__(self):
        ModuleBase.__init__(self)
        self.name = "Lung Segmentation"

    def segmentation(self):
        ''

    def extract_lung(self):
        threshold = sitk.ThresholdImageFilter()
        threshold.GetUpper()
        t_image = threshold.Execute(self.get_input())
        print(t_image)

    def remove_rim(self):
        ''
