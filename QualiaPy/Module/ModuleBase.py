import threading

__author__ = 'wchoi'


class ModuleBase(threading.Thread):
    # Module Base class

    name = ''
    inputs = []
    outputs = []

    def __init__(self):
        threading.Thread.__init__(self)

    def set_input(self, input_image, i=0):
        if len(self.inputs) > i:
            self.inputs[i] = input_image
        else:
            self.inputs.append(input_image)
        print(self.inputs, i, input_image)

    def get_input(self, i=0):
        if len(self.inputs) > i:
            return self.inputs[i]

    def get_output(self, i=0):
        if len(self.outputs) > i:
            return self.outputs[i]

    def run(self):
        for i in range(10000):
            print('test', i)
        self.outputs = self.inputs
